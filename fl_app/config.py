import os


basedir = os.path.abspath(os.path.dirname(__file__))

DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}:{port}/'.format(user='postgres',
                                                                   pw=PASSWORD_DB,
                                                                   url='database-1.cltvhcdtllm0.us-east-2.rds.amazonaws.com',
                                                                   port = '5432')

SQLALCHEMY_DATABASE_URI = DB_URL
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

CACHE_TYPE = "simple"
CACHE_DEFAULT_TIMEOUT =  300