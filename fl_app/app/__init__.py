from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_caching import Cache 
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker



app = Flask(__name__)
app.config.from_object('config')

cache = Cache(app)
db = SQLAlchemy(app)


from app import views, models