import json

from app import app, cache, db
import pdb
from flask import jsonify, request
from app.models import Product, Reviews
from flask import make_response
from sqlalchemy import update




PAGE_COUNT= 3

@app.errorhandler(404)
def not_found(*c,**d):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(400)
def bad_request(*c,**d):
    return make_response(jsonify({'error': 'Not found'}), 400)


@app.route('/<int:product_id>', methods=['GET'])
@app.route('/<int:product_id>/page=<int:page_id>', methods=['GET'])
@cache.cached()
def get_product(product_id, page_id = 1):

    product = Product.query.get(product_id)
    if product  is None:
        return not_found()

    reviews = [ {'title':i.title,'review':i.review, 'id':i.id} for i in product.reviews.paginate(page_id, PAGE_COUNT, False).items]

    return jsonify({'id': product.id, 'asin':product.asin, 'title':product.title,'reviews':reviews})


@app.route('/update_review/<int:review_id>', methods=['PUT'])
def update_reviews(review_id):

    columns = {'title', 'reviews', 'asin'}
    data = json.loads(request.json)
    values = columns & set(data)

    if not values:
        return bad_request()

    
    review = Reviews.query.get(review_id)

    if review is None:
        review = Reviews(**{column:data[column] for column in values})
        db.session.add(review)
        db.session.commit()
        return json.dumps({'success':True}), 201, {'ContentType':'application/json'} 

    for column in values: #update это свойство класса, а не атрибут экземпляра, поэтому не юзаем update 
        setattr(review, column, data[column])
    
    db.session.add(review)
    db.session.commit()

    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 