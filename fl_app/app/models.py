from app import db
import json

class Reviews(db.Model):

    id = db.Column(db.Integer, primary_key = True)
    asin = db.Column(db.String(10), db.ForeignKey('product.asin'), index = True)
    title = db.Column(db.Text)
    review = db.Column(db.Text)

    def __repr__(self):
        return '<asin %s>:\t%s \n\t Reviews: %s ' % (self.asin, self.title, self.review)

class Product(db.Model):
    
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.Text)
    asin = db.Column(db.String(10), index = True, unique = True)
    reviews = db.relationship('Reviews', backref='product', lazy = 'dynamic')
    
    def __repr__(self):
        return '<asin %s>:\t%s  ' % (self.asin, self.title)