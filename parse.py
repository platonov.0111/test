import os
import re
import csv
import requests

import psycopg2

CONFIG_DB = {'user':'postgres', 
             'password':PASSWORD_DB
             'host':'database-1.cltvhcdtllm0.us-east-2.rds.amazonaws.com',
             'port':"5432"}

class Test_read_google():


    def __init__(self, link, first_line_is_header:bool, table_name = None, use_filename_as_table_name:bool = False, auto_name_columns:bool = False):
        """
        link - текс, предположительно являющийся ссылкой на csv документ.
            - Допускается ссылка любого вида на ресурс docs.google.com/spreadsheets/,
            - либо прямая ссылка на файл, типа https://domain.com/file.csv,
            - либо адрес в файловой системе

        first_line_is_header - является ли первая строка заголовками таблицы

        table_name - имя таблицы, БД в которую запишутся данные. Имя на существующую таблицу, либо название таблицы, которая будет создана

        use_filename_as_table_name - При активном флаге, в случае, если таблица не сущестует,
        при ее создании в качестве названия будет использовано название файла

        auto_name_columns - При создании таблицы, если первая строка таблицы не заголовки, 
        в качестве названия столбцов будут использовны стандартные названия Column1, Column2
        """

        self.link = link
        if use_filename_as_table_name:
            self.table_name = None
        else:
            self.table_name = table_name
            assert self.table_name, "Empty table name. Set either table_name or use_filename_as_table_name " 
        self.first_line_is_header = first_line_is_header
        self.use_filename_as_table_name = use_filename_as_table_name
        self.auto_name_columns = auto_name_columns
        self.columns_header = []
        self.data = []
        self.filename = None
        self.__connection_db = None #Вроде бы, по заданию, строго прятаться не надо, то инкапсуляция через подчеркивания, а не magic методы
        self.__cursor = None



    def active_connect(func):
        def wrapper(self, *c,**d):

            if not (self.__connection_db and self.__connection_db.status) :
                self._connec_db()

            return func(self,*c,**d)

        return wrapper

    def read_sheets(self):
        """
        Считывает файл по ссылке, и возвращает кортеж из данных и названия файла
        """

        url = self._get_download_link(self.link)
        try:

            response = requests.get(url)
            csvfile = csv.reader(response.text.replace('\x00', '').splitlines())
            data = [tuple(row) for row in csvfile]

            head = response.headers.get("Content-Disposition","")
            rez = re.findall(r"""filename=['"](.*?)['"];""",head)
            if rez[0]:
                self.filename = re.sub(r'(?:\W*?|^\d*|csv)', '', rez[0]).lower()

        except requests.exceptions.InvalidSchema: 

            with open(url, newline='', encoding='utf-8') as csvfile:
                csvfile = csv.reader(csvfile)
                data = [tuple(row) for row in csvfile]
            self.filename  = os.path.basename(url).replace(".csv","").lower()
        
        if self.first_line_is_header:
            self.columns_header = [i for i in data[0]]
            self.data = data[1:]
        else:
            self.data = data

        if self.use_filename_as_table_name:
            self.table_name = self.filename


    def _get_download_link(self,text = None):
        """
        Проверяет является ли текст ссылкой на скачивание csv.
        1. В ссылах на гугл таблицы пытается отыскать id и формирует ссылку на скачивание
        2. Либо просто проверяет является ли текст ссылкой на скачивание, либо путём в файловой системе
        """

        if text.find("https://docs.google.com/spreadsheets/") != -1:
            result = re.findall(r'/([-_\w]{44})/',text)
            assert result , "unknow format google.doct's link"
            
            return 'https://docs.google.com/spreadsheets/d/{key}/export?format=csv&id={key}&gid=0'.format(key = result[0])

        result = re.findall(r'^(?:http|ftp|sftp|[A-Z]{1}:).*\.csv$',text)
        assert result , "unknow link "
        
        return result[0]


    def _connec_db(self):
        
        try:
            self.__connection_db = psycopg2.connect(user=CONFIG_DB['user'], 
                                                    password=CONFIG_DB['password'],
                                                    host=CONFIG_DB['host'],
                                                    port = CONFIG_DB['port'])
            self.__cursor = self.__connection_db.cursor()                             
        except psycopg2.OperationalError as e :
            print(e)
            print("Error Establishing a Database Connection")

    @active_connect
    def __table_is_exists(self):

        self.__cursor.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema='public'""")

        return self.table_name in set(i[0] for i in self.__cursor.fetchall())

    @active_connect
    def _create_table(self):

        if self.auto_name_columns:
            self.columns_header = ['Column{i+1}' for i in range(len(self.data[0]))]

        elif not self.first_line_is_header:
            print("example")
            print(filename)
            print(', '.join(f"Column{i+1}" for i in range(len(self.data[0]))))

            for i in data[:5]:
                print(','.join( (len(y) > 20 and (y[:17]+"...") or (y + " " * (20 - len(y)))) for y in i))

            print('...')
            print("\nenter name of the columns\n")

            self.columns_header = [input(f'Column{i+1}: ') for i in range(len(self.data[0]))]

        # далее идет весьма тупое решение, где тип всех столбцов берется как текст,
        # но мы в целом строим структуру БД в слепую, так что без странных решений тут будет сложно 
        self.__cursor.execute("CREATE TABLE {table_name} ( id SERIAL PRIMARY KEY, {columns}  )".format(table_name = self.table_name, \
                                                                                columns = ",".join(i + " text" for i in self.columns_header) ))
        self.__connection_db.commit()


    @active_connect
    def write_data(self):
        """
        Записывает данные из итерируемого объета в таблицу БД.
        Порядок столбцов и их тип должен совпадать в источнике и таблице
        """

        self.__cursor.execute("SELECT column_name FROM information_schema.columns WHERE table_name = %s",[self.table_name])
        columns = ','.join(i[0] for i in self.__cursor.fetchall()[1:])
        records_list_template = ','.join(['%s'] * len(self.data))

        self.__cursor.execute(" INSERT INTO {table_name} ({columns})  VALUES {values} ".format(table_name = self.table_name, columns = columns, values = records_list_template), self.data)
        self.__connection_db.commit()


    def parse(self):
        """
        Читает csv файл и записывает результат в таблицу БД.
        Имя таблицы передается в table_name
        Если флаг use_filename_as_table_name активный, то вместо table_name будет использовано название файла
            - Из названия файла будут выброшены все не числа и не символы; из начала названия будут убраны цифры

        Если таблица не существует, то она будет создана. 
            - В качестве названия столбцов будут использованы заголовки таблицы
            - Если заголовков нет,
                и auto_name_columns == True то названия будут Column1, Column2...
                либо название столбцов придется ввести руками через консоль

        В сущестующую таблицу данные будут дописываться
        """
        self.read_sheets()

        if not self.__table_is_exists():
            self._create_table()
        
        self.write_data()





if __name__ == '__main__':

    x = Test_read_google(r"https://docs.google.com/spreadsheets/d/1iSR0bR0TO5C3CfNv-k1bxrKLD5SuYt_2HXhI2yq15Kg/edit#gid=0", table_name = "reviews", first_line_is_header = True)
    d = x.parse()

